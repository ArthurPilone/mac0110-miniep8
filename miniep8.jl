#Por Arthur Pilone. NUSP 11795450

function compareByValue(a,b)
    v = ['2','3','4','5','6','7','8','9','1','J','Q','K','A'];
    return findfirst(isequal(a[1]),v) < findfirst(isequal(b[1]),v)
end

function compareByValueAndSuit(a,b)
    v = ['♢','♠','♡','♣']
    if(a[length(a)] == b[length(b)])
        return compareByValue(a,b)
    else
        return findfirst(isequal(a[length(a)]),v) < findfirst(isequal(b[length(b)]),v)
    end
end

# O trecho abaixo reutiliza parte do código feito em
# aula, mas com as modificações a serem propostas neste miniEP

function troca(v, i, j)
    aux = v[i]
    v[i] = v[j]
    v[j] = aux
end

function insercao(v)
    tam = length(v)
    for i in 2:tam
        j = i
        while j > 1
            if compareByValueAndSuit(v[j],v[j - 1])
                troca(v, j, j - 1)
            else
                break
            end
            j = j - 1
        end
    end
    return v
end

##Testes##
using Test

function testByValue()
    @test compareByValue("2♠","A♠")
    @test !compareByValue("A♠","K♠")
    @test !compareByValue("K♡","10♡")
    @test compareByValue("J♢","Q♡")
    @test compareByValue("7♠","Q♡")
    @test !compareByValue("10♠","10♡")
    @test !compareByValue("A♢","A♡")
    @test compareByValue("4♢","Q♡")
    @test compareByValueAndSuit("2♠","A♠")
    @test !compareByValueAndSuit("K♡","10♡")
    @test compareByValueAndSuit("10♠","10♡")
    @test compareByValueAndSuit("A♠","2♡")
    @test !compareByValueAndSuit("2♡","10♢")
    @test !compareByValueAndSuit("7♡","A♠")
    @test insercao(["10♡", "3♢","J♠","Q♠","K♠","A♠","2♠"]) == ["3♢","2♠","J♠","Q♠","K♠","A♠","10♡"]
    @test insercao(["10♡", "10♢", "K♠","A♠","J♠","A♠"]) == ["10♢","J♠","K♠","A♠","A♠","10♡"]
    @test insercao(["A♣","6♠","5♠","2♢","K♡","3♡"]) == ["2♢","5♠","6♠","3♡","K♡","A♣"]
    println("Fim dos testes")
end
testByValue()
